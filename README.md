# flutter_mvc

Example project using an MVC like design pattern using rxDart and the provider packages

## Provider
https://www.youtube.com/watch?v=BulIREvHBWg

https://www.youtube.com/watch?v=d_m5csmrf7I

## StreamBuilder
https://api.flutter.dev/flutter/widgets/StreamBuilder-class.html


## Getting Started

https://pub.dev/packages/rxdart

https://pub.dev/packages/provider 

This project is a starting point for a Flutter application.

A few resources to get you started if this is your first Flutter project:

- [Lab: Write your first Flutter app](https://flutter.dev/docs/get-started/codelab)
- [Cookbook: Useful Flutter samples](https://flutter.dev/docs/cookbook)

For help getting started with Flutter, view our
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.
