import 'package:mvctemplate/model/counter_service.dart';
import 'package:rxdart/rxdart.dart';

class CounterController {
  final CounterService _service = CounterService();

  final BehaviorSubject<int> countSubject = BehaviorSubject.seeded(0);
  // the UI listens to changes on this stream
  ValueStream<int> get countStream => countSubject.stream;

  void increment() async {
    // fetch data from backend
    final newCount = await _service.increment(countSubject.value);

    // add data in the subject to update the stream
    countSubject.add(newCount);
  }
}
