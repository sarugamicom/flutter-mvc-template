class CounterService {
  int _count = 0;

  Future<int> increment(int currentCount) async {
    //Do API stuff
    return _count += 1;
  }
}
