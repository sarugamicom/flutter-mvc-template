import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:mvctemplate/controller/counter_controller.dart';
import 'package:provider/provider.dart';

class CounterPage extends StatelessWidget {
  CounterPage({Key key}) : super(key: key);

  void _incrementCounter(BuildContext context) {
    // tell controller to update the counter
    context.read<CounterController>().increment();
  }

  @override
  Widget build(BuildContext context) {
    print('Build Screen');
    return Scaffold(
      appBar: AppBar(
        title: Text('Counter'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'You have pushed the button this many times:',
            ),
            CounterText()
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () => _incrementCounter(context),
        tooltip: 'Increment',
        child: Icon(Icons.add),
      ),
    );
  }
}

class CounterText extends StatelessWidget {
  const CounterText({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    // important to use listen false, because we dont want to update on changes of the counterController (default provider behaviour) but only listen to the stream
    // this can alse be set in the initState method on a statefulwidget with context.read<CounterController>()
    final countStream = Provider.of<CounterController>(context, listen: false).countStream;

    // create a streambuilder to listen to changes in the data
    return StreamBuilder<int>(
      stream: countStream,
      initialData: 0,
      builder: (BuildContext context, AsyncSnapshot<int> snapshot) {
        return Text(
          '${snapshot.data}',
          style: Theme.of(context).textTheme.headline4,
        );
      },
    );
  }
}
